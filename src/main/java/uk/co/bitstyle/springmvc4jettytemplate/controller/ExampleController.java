package uk.co.bitstyle.springmvc4jettytemplate.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import uk.co.bitstyle.springmvc4jettytemplate.application.EchoService;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Handles requests for the application home page.
 */
@Controller
@ComponentScan("uk.co.bitstyle.springmvc4jettytemplate.application")
public class ExampleController {

    private static final Logger logger = LoggerFactory.getLogger(ExampleController.class);

    @Autowired
    private EchoService echoService;

    /**
     * Simple controller for "/example" that returns a Thymeleaf view.
     */
    @RequestMapping(value = "/example", method = RequestMethod.GET)
    public String home(Locale locale, Model model) {
        logger.info("Welcome home! The client locale is " + locale.toString());

        Date date = new Date();
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

        String formattedDate = dateFormat.format(date);
        model.addAttribute("serverTime", formattedDate);

        model.addAttribute("echoService", echoService);

        model.addAttribute("someItems", new String[] { "one", "two", "three" });

        return "example";
    }

}
