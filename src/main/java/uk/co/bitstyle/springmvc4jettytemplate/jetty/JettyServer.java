package uk.co.bitstyle.springmvc4jettytemplate.jetty;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.webapp.WebAppContext;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import java.io.IOException;

/**
 * This houses and controls web server that will run the project's web app.
 *
 * @author chrisspiking
 */
public class JettyServer {

    private static final Logger logger = LoggerFactory.getLogger(JettyServer.class);
    private static final int DEFAULT_PORT = 8080;
    private static final String CONFIG_LOCATION = "uk.co.bitstyle.springmvc4jettytemplate.config";

    private final Object serverStateLock = new Object();

    private final int webServerPort;
    private final Server webServer;

    private boolean serverStarted = false;

    public JettyServer(int port) throws JettyServerException {
        webServerPort = port;
        webServer = new Server(webServerPort);
        webServer.setStopAtShutdown(true);
        try {
            webServer.setHandler(getServletContextHandler(getContext()));
        } catch (IOException e) {
            throw new JettyServerException("Cannot instantiate JettyServer instance", e);
        }
    }

    public void start() throws JettyServerException {
        synchronized (serverStateLock) {
            if(!serverStarted) {
                attemptToStartWebServer();
                logger.info("Server started at port {}", webServerPort);
                serverStarted = true;
            } else {
                throw new IllegalStateException("JettyServer instance already started on port " + webServerPort);
            }
        }
    }

    @SuppressWarnings("unused")
    public void stop() throws JettyServerException {
        synchronized (serverStateLock) {
            if(serverStarted) {
                attemptToStopWebServer();
                logger.info("Server stopped at port {}", webServerPort);
                serverStarted = false;
            } else {
                throw new IllegalStateException("JettyServer instance already stopped for port " + webServerPort);
            }
        }
    }

    private void attemptToStartWebServer() {
        try {
            logger.debug("Starting server at port {}", webServerPort);
            webServer.start();
        } catch (Exception ex) {
            throw new JettyServerException("Unable to successfully start web server.", ex);
        }
    }

    private void attemptToStopWebServer() {
        try {
            logger.debug("Stopping server at port {}", webServerPort);
            webServer.stop();
        } catch (Exception ex) {
            throw new JettyServerException("Unable to successfully stop web server.", ex);
        }
    }

    private ServletContextHandler getServletContextHandler(WebApplicationContext context) throws IOException {

        WebAppContext contextHandler = new WebAppContext();
        contextHandler.setContextPath("/");

        contextHandler.addServlet(new ServletHolder(new DispatcherServlet(context)), "/");
        contextHandler.addEventListener(new ContextLoaderListener(context));
        contextHandler.setResourceBase(new ClassPathResource("webapp").getURI().toString());

        contextHandler.setInitParameter("org.eclipse.jetty.servlet.Default.dirAllowed", "false");

        return contextHandler;
    }

    private WebApplicationContext getContext() {
        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        context.setConfigLocation(CONFIG_LOCATION);
        return context;
    }

    public static void main(String args[]) {
        JettyServer server = new JettyServer(getPortFromArgs(args));
        server.start();
    }

    private static int getPortFromArgs(String[] args) {
        if (args.length > 0) {
            try {
                return Integer.valueOf(args[0]);
            } catch (NumberFormatException ignore) {
            }
        }
        logger.debug("No server port configured, falling back to {}", DEFAULT_PORT);
        return DEFAULT_PORT;
    }
}
