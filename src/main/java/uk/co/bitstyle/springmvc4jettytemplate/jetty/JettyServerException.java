package uk.co.bitstyle.springmvc4jettytemplate.jetty;

/**
 * @author chrisspiking
 */
public class JettyServerException extends RuntimeException {
    public JettyServerException(String message, Throwable ex) {
        super(message, ex);
    }
}
