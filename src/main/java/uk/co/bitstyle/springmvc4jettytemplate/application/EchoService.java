package uk.co.bitstyle.springmvc4jettytemplate.application;

import org.springframework.stereotype.Service;

/**
 * A service to show method invokation in ThymeLeaf.
 */
@Service
public class EchoService {

    public String printMe(String msg) {
        return new String(msg);
    }

}
