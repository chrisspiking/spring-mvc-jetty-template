## Spring 4 MVC / Embedded Jetty WebApp Template ##

### What is this repository for? ###

* This is a template that you can download to get you quickly up and running using Spring and Jetty to get yourself an annotatively configured Spring MVC WebApp, using thymeleaf as the view resolver.
